#!/bin/bash

set -eo pipefail

mvn build-helper:parse-version versions:set -DgenerateBackupPoms=false -DnewVersion=\${parsedVersion.majorVersion}.\${parsedVersion.minorVersion}.\${parsedVersion.nextIncrementalVersion}

git config --global user.email "stan@codesmith.ch"
git config --global user.name "Stanislas Nanchen (gitlab)"

git commit -am "next release"
git push origin HEAD:main
