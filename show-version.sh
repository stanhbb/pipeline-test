#!/bin/bash

export REL_GIT_COUNT=$(git rev-list HEAD --count)
export REL_REVISION="1.${REL_GIT_COUNT}.${REL_RELEASE:-0}"

mkdir -p .mvn
cat <<EOF >.mvn/maven.config
-Drevision=${REL_REVISION}
EOF

mvn help:evaluate -Dexpression=project.version
mvn install

rm -fr .mvn
